package com.mastercard.ds.dsvinetratrainingservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DsVinetraTrainingServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(DsVinetraTrainingServiceApplication.class, args);
	}

}
